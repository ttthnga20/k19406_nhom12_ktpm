const { I } = inject()
const apiKeysPageFunction = require('../page/createAPIKeysPage/index');
const Myfunctions = require('../page/common/functions')
const MyVariable = require('../page/common/variable')
const apiName = Myfunctions.randomString(3)
const editApiName = 'API19406'
Given('I create API Keys', () => {
    apiKeysPageFunction.createAPIKeys(apiName)
});
Given('I edit API Keys', () => {
    apiKeysPageFunction.editAPIKeys(editApiName)
});
Given('I delete API Keys', () => {
    apiKeysPageFunction.deleteAPIKeys()
});
