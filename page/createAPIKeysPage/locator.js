module.exports = {
    APIKeysTitle: "//h4[text()='API Keys']",
    createAPIButton: "//button//span[text()='Tạo API key']",
    addAPITitle: "//h4[text()='Thêm API Key']",
    apiNameField: "//input[contains(@class,'mat-input-element')]",
    cancleApiButton: "//button//span[text()='HỦY']",
    acpCreateAPIButton: "//button//span[text()=' Tạo và xem API Key ']",
    completeTilte: "//h4[text()='API Key đã được khởi tạo thành công']",
    completeButton: "//button//span[text()='Xong']",
    editButton: "//mat-icon[text()='edit']",
    editTitle: "//h4[text()='Sửa API Key']",
    acpEditButton: "//span[text()=' Lưu thay đổi ']",
    deleteAPIButton: "//mat-icon[text()='delete']",
    acpDeleteAPIButton: "//button//span[text()=' Đồng ý ']",
    apiNameLable: "//div[@style='font-size: 16px;']",
    //apiSuccessfulDelete: "//div[@aria-label='Xóa API Key thành công' and text()='Xóa API Key thành công']"

}
