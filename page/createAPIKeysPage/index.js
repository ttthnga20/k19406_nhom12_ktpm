const { I } = inject()
const timeout = require('../common/timeout')
const settingPageLocator = require('../settingPage/locator')
const apiKeysPageLocator = require('./locator')
const customMethod = require("../../utils/customMethod")

module.exports = {
    viewAPIKeysPage() {
        customMethod.clickElement(settingPageLocator.hambergerButton)
        customMethod.clickElement(settingPageLocator.settingTab)
        customMethod.clickElement(settingPageLocator.apiKeysTab)
        I.waitForElement(apiKeysPageLocator.APIKeysTitle)
    },
    createAPIKeys(apiName) {
        I.click(apiKeysPageLocator.createAPIButton)
        I.waitForElement(apiKeysPageLocator.addAPITitle)
        I.fillField(apiKeysPageLocator.apiNameField, apiName)
        I.click(apiKeysPageLocator.acpCreateAPIButton)
        I.waitForElement(apiKeysPageLocator.completeTilte)
        I.click(apiKeysPageLocator.completeButton)
        I.waitForElement(apiKeysPageLocator.APIKeysTitle)
        I.seeTextEquals(apiName, apiKeysPageLocator.apiNameLable)
    },
    editAPIKeys(editApiName) {
        I.click(apiKeysPageLocator.editButton)
        I.waitForElement(apiKeysPageLocator.editTitle)
        I.fillField(apiKeysPageLocator.apiNameField, editApiName)
        I.click(apiKeysPageLocator.acpEditButton)
        I.waitForElement(apiKeysPageLocator.APIKeysTitle)
        I.seeTextEquals(editApiName, apiKeysPageLocator.apiNameLable)
    },
    deleteAPIKeys() {
        I.click(apiKeysPageLocator.deleteAPIButton)
        I.click(apiKeysPageLocator.acpDeleteAPIButton)
        I.waitForInvisible(apiKeysPageLocator.deleteAPIButton)
        I.see('Xóa API Key thành công')
    }
}
